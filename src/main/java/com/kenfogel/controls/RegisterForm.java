package com.kenfogel.controls;

import java.awt.Color;
import java.io.Serializable;
import java.text.DateFormatSymbols;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.model.SelectItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Named("form")
@SessionScoped
public class RegisterForm implements Serializable {

    private final static Logger LOG = LoggerFactory.getLogger(RegisterForm.class);
    
    public enum Education {

        HIGH_SCHOOL, BACHELOR, MASTER, DOCTOR
    };

    public static class Weekday {

        private final int dayOfWeek;

        public Weekday(int dayOfWeek) {
            this.dayOfWeek = dayOfWeek;
        }

        public String getDayName() {
            DateFormatSymbols symbols = new DateFormatSymbols();
            String[] weekdays = symbols.getWeekdays();
            return weekdays[dayOfWeek];
        }

        public int getDayNumber() {
            return dayOfWeek;
        }
    }

    private final SelectItem[] colorItems = {
        new SelectItem(Color.RED.getRGB(), "Red"), // value, label
        new SelectItem(Color.GREEN.getRGB(), "Green"),
        new SelectItem(Color.BLUE.getRGB(), "Blue"),
        new SelectItem(Color.YELLOW.getRGB(), "Yellow"),
        new SelectItem(Color.ORANGE.getRGB(), "Orange", "", true) // disabled
    };

    private static final Map<String, Education> EDUCATION_ITEMS;

    static {
        EDUCATION_ITEMS = new LinkedHashMap<>();
        EDUCATION_ITEMS.put("High School", Education.HIGH_SCHOOL); // label, value
        EDUCATION_ITEMS.put("Bachelor's", Education.BACHELOR);
        EDUCATION_ITEMS.put("Master's", Education.MASTER);
        EDUCATION_ITEMS.put("Doctorate", Education.DOCTOR);
    }

    private static final SelectItem[] LANGUAGE_ITEMS = {new SelectItem("English"),
        new SelectItem("French"), new SelectItem("Russian"),
        new SelectItem("Italian"),
        new SelectItem("Esperanto", "Esperanto", "", true)};

    private static final Collection<SelectItem> BIRTH_YEARS;

    static {
        BIRTH_YEARS = new ArrayList<>();
        // The first item is a "no selection" item
        BIRTH_YEARS.add(new SelectItem(null, "Pick a year:", "", false, false,
                true));
        for (int i = 1900; i < 2020; ++i) {
            BIRTH_YEARS.add(new SelectItem(i));
        }
    }

    private static final Weekday[] DAYS_OF_THE_WEEK;

    static {
        DAYS_OF_THE_WEEK = new Weekday[7];
        for (int i = Calendar.SUNDAY; i <= Calendar.SATURDAY; i++) {
            DAYS_OF_THE_WEEK[i - Calendar.SUNDAY] = new Weekday(i);
        }
    }

    private String name;
    private boolean contactMe;
    private int[] bestDaysToContact;
    private Integer yearOfBirth;
    private int[] colors;
    private Set<String> languages = new TreeSet<>();
    private Education education = Education.BACHELOR;

    public String getName() {
        return name;
    }

    public void setName(String newValue) {
        name = newValue;
    }

    public boolean getContactMe() {
        return contactMe;
    }

    public void setContactMe(boolean newValue) {
        contactMe = newValue;
    }

    public int[] getBestDaysToContact() {
        return bestDaysToContact;
    }

    public void setBestDaysToContact(int[] newValue) {
        bestDaysToContact = newValue;
    }

    public Integer getYearOfBirth() {
        return yearOfBirth;
    }

    public void setYearOfBirth(Integer newValue) {
        yearOfBirth = newValue;
    }

    public int[] getColors() {
        return colors;
    }

    public void setColors(int[] newValue) {
        colors = newValue;
    }

    public Set<String> getLanguages() {
        return languages;
    }

    public void setLanguages(Set<String> newValue) {
        languages = newValue;
    }

    public Education getEducation() {
        return education;
    }

    public void setEducation(Education newValue) {
        education = newValue;
    }

    public Collection<SelectItem> getYearItems() {
        return BIRTH_YEARS;
    }

    public Weekday[] getDaysOfTheWeek() {
        return DAYS_OF_THE_WEEK;
    }

    public SelectItem[] getLanguageItems() {
        return LANGUAGE_ITEMS;
    }

    public SelectItem[] getColorItems() {
        return colorItems;
    }

    public Map<String, Education> getEducationItems() {
        return EDUCATION_ITEMS;
    }

    public String getBestDaysConcatenated() {
        return Arrays.toString(bestDaysToContact);
    }

    public String getColorsConcatenated() {
        StringBuilder result = new StringBuilder();
        for (int color : colors) {
            result.append(String.format("%06x ", color));
        }
        return result.toString();
    }

}
